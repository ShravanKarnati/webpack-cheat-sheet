const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    entry : ["./src/js/index.js","./src/css/index.css"],

    module : {
        rules : [

            {
                test : /\.html$/,
                use : [
                    "html-loader",
                ]
            },

            {
                test : /\.(jpg|jpeg|png|gif|icon|svg)$/,
                use : {
                    loader : "file-loader",
                    options : {
                        esModule : false,
                        name : "[name].[ext]",
                        outputPath : "./img/"
                    }
                },
            },
        ]
    },
    plugins : [
        new HtmlWebpackPlugin ( {
            template : './src/index.html',
        }),
    ],
}