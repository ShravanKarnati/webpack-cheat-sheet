const path = require("path");
const common = require("./webpack.common");
const merge = require("webpack-merge");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require("terser-webpack-plugin");

module.exports = merge(common,{
    mode : "production",
    output : {
        path : path.resolve(__dirname,"./dist"),
        filename : "js/main.[contentHash].js",
    },
    plugins : [
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename : "style.[contentHash].css",
        }),
    ],

    optimization : {
        minimizer : [
            new OptimizeCssAssetsWebpackPlugin(),
            new TerserPlugin(), 
        ]
    },

    module : {
        rules : [
            {
                test : /\.css$/,
                use : [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "postcss-loader",
                ]
            },

            {
                test : /\.scss$/,
                use : [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "postcss-loader",
                    "sass-loader",
                ],
            },
        ]
    },
});