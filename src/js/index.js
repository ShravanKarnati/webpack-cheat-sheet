
// CSS can be imported this way or can be specified as entry in the webpack config file. 
import '../css/index.css';

// Images can be imported this way --> explicitly importing image by image when needed.  
import "../images/1.png";   
import "../images/2.png";
import "../images/3.png";
import "../images/4.png";
import "../images/5.png";
import "../images/6.png";


// Or Use this function to import all images in a folder.

function importAll(r) {
    return r.keys().map(r);
    }
    
const images = importAll(require.context('../images/', false, /\.(png|jpe?g|svg|ico|gif)$/)); // '../images/' is the folder.