const path = require("path");
const common = require("./webpack.common");
const merge = require("webpack-merge");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");


module.exports = merge(common,{
    mode : "development",
    output : {
        path : path.resolve(__dirname,"./dist"),
        filename : "js/main.bundle.js",
    },
    plugins : [
        new MiniCssExtractPlugin({
            filename : "style.css",
        }),
    ],
    module : {
        rules : [

            {
                test : /\.css$/,
                use : [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                ]
            },

            {
                test : /\.scss$/,
                use : [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "sass-loader",
                ],
            },
        ]
    },
});